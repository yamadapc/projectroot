# projectroot
A simple command-line utility for finding the current project's root.

Borrows lightly from emacs' projectile plugin.

## License
This code is licensed under the GPLv2 license for Pedro Tacla Yamada. Please
refer to the [LICENSE](/LICENSE) file for more information.
